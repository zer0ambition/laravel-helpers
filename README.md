# Laravel Helpers 
This plugin provide set of base classes, interfaces and traits.

## Installation
### Composer
1. Add to required `"zer0ambition/laravel-helpers": "master-dev"`
1. Run `composer update`
