<?php


namespace ZeroAmbition\Helpers\Constants\Http;

class RequestConstants
{
    /**
     * @var int
     */
    const DEFAULT_LIMIT = 15;

    /**
     * @var array
     */
    const SORT_DIRECTIONS = [
        'asc', 'desc'
    ];

    /**
     * @var string
     */
    public const DEFAULT_SORT_DIRECTION = 'asc';
}
