<?php

namespace ZeroAmbition\Helpers\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;
use ZeroAmbition\Helpers\Http\Response\ErrorResponse;

class JWTRefreshAuthenticate
{
    /**
     * Check if JWT token expired
     *
     * Try to make and send new token
     *
     * @param  Request $request
     * @param  Closure $next    Next responses
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        try {
            try {
                $user = JWTAuth::setRequest($request)->parseToken()->authenticate();
                if (!$user) {
                    return new ErrorResponse(['message' => trans('auth.unauth')], ResponseAlias::HTTP_UNAUTHORIZED);
                }
            } catch (TokenBlacklistedException $e) {
                return new ErrorResponse(['errors' => $e->getMessage(), 'message' => trans('auth.unauth')], ResponseAlias::HTTP_UNAUTHORIZED);
            }
        } catch (TokenExpiredException $e) {
            $token = JWTAuth::refresh(JWTAuth::getToken());
            // send the refreshed token back to the client
            header("Authorization: Bearer $token");
            $user = JWTAuth::setToken($token)->toUser();
        } catch (JWTException $e) {
            return new ErrorResponse(['errors' => $e->getMessage(), 'message' => trans('auth.unauth')], ResponseAlias::HTTP_UNAUTHORIZED);
        }

        auth()->login($user);

        return $next($request);
    }
}
