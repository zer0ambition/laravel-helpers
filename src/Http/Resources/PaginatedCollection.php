<?php

namespace ZeroAmbition\Helpers\Http\Resources;

use ZeroAmbition\Helpers\Interfaces\Http\Resources\IResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use JetBrains\PhpStorm\ArrayShape;

class PaginatedCollection extends ResourceCollection
{
    /**
     * @var string $resourceClass
     */
    private string $resourceClass;

    /**
     * PaginatedCollection constructor.
     *
     * @param $resource
     * @param string $resourceClass
     */
    public function __construct($resource, string $resourceClass)
    {
        parent::__construct($resource);
        $this->resourceClass = $resourceClass;
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    #[ArrayShape(['items' => "", 'total' => "mixed", 'page' => "mixed", 'limit' => "mixed", 'lastPage' => "mixed"])]
    public function toArray($request) : array
    {
        /**
         * @var IResource
         */
        $resourceCollection = (new $this->resourceClass($this->collection));
        $fields = $request->input('fields', []);
        return [
            'items' => $resourceCollection->only(!empty($fields) ? $fields : $resourceCollection->getAttributes()),
            'total' => $this->resource->total(),
            'page' => $this->resource->currentPage(),
            'limit' => $this->resource->perPage(),
            'lastPage' => $this->resource->lastPage()
        ];
    }
}
