<?php


namespace ZeroAmbition\Helpers\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use ZeroAmbition\Helpers\Constants\Http\RequestConstants;

abstract class BaseFilter
{
    /**
     * @var string
     */
    public string $sortDirection;

    /**
     * @var Builder $builder
     */
    protected Builder $builder;

    /**
     * @var Request $request
     */
    protected Request $request;

    /**
     * BaseFilter constructor.
     *
     * @param Builder   $builder
     * @param Request $request
     */
    public function __construct(Builder $builder, Request $request)
    {
        $this->builder       = $builder;
        $this->request       = $request;
        $this->sortDirection = $request->sortDirection ?? RequestConstants::DEFAULT_SORT_DIRECTION;
    }

    /**
     * Apply filters
     *
     * @return Builder
     */
    public function apply(): Builder
    {
        foreach ($this->filters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }

        return $this->builder;
    }

    /**
     * Get filters from request
     *
     * @return array
     */
    public function filters(): array
    {
        return $this->request->all();
    }
}
