<?php

namespace ZeroAmbition\Helpers\Interfaces\Http\Filters;

interface SortsByRelations
{
    /**
     * Returns relations map
     * [rel => callable]
     *
     * @return array
     */
    public function getRelationsSortMap(): array;

    /**
     * Add to builder query from relation sort map
     *
     * @param string $value
     * @return void
     */
    public function sortByRelation(string $value);
}
