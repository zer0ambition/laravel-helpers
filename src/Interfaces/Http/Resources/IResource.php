<?php


namespace ZeroAmbition\Helpers\Interfaces\Http\Resources;


interface IResource
{
    /**
     * Get resource fields with values
     *
     * @return array
     */
    public function getFields() : array;

    /**
     * @return array
     */
    public function getAttributes() : array;
}
