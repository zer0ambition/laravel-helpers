<?php


namespace ZeroAmbition\Helpers\Traits\Http\Resources;


trait HasAttributes
{
    /**
     * @return array
     */
    public function getAttributes() : array
    {
        return array_keys($this->getFields());
    }
}
