<?php


namespace ZeroAmbition\Helpers\Traits\Http\Requests;


use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use ZeroAmbition\Helpers\Http\Response\ErrorResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

trait Throwable
{
    /**
     * Handle a failed validation attempt.
     *
     * @param  Validator $validator
     * @return void
     *
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            new ErrorResponse(
                [
                    'errors' => $validator->errors(),
                    'message' => trans('http.wrong_data')
                ],
                ResponseAlias::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
