<?php


namespace ZeroAmbition\Helpers\Traits\Http\Requests;

use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;
use ZeroAmbition\Helpers\Constants\RequestConstants;

trait HasPaginationInterface
{
    /**
     * Get pagination request interface rules
     *
     * @param  array $fields
     * @return array
     */
    #[ArrayShape(['fields' => "string", 'fields.*' => "\Illuminate\Validation\Rules\In", 'page' => "string", 'limit' => "string", 'sortBy' => "\Illuminate\Validation\Rules\In", 'sortDirection' => "\Illuminate\Validation\Rules\In"])]
    public function getPaginationRules(array $fields) : array
    {
        return [
            'fields' => 'array',
            'fields.*' => Rule::in($fields),
            'page' => 'numeric|min:1',
            'limit' => 'numeric',
            'sortBy' => Rule::in($fields),
            'sortDirection' => Rule::in(RequestConstants::SORT_DIRECTIONS)
        ];
    }
}
