<?php

namespace ZeroAmbition\Helpers\Traits\Http\Filters;

trait RelationsSorting
{
    /**
     * Returns relations map
     * [rel => callable]
     *
     * @return array
     */
    abstract public function getRelationsSortMap(): array;

    /**
     * Add to builder query from relation sort map
     *
     * @param string $value
     * @return void
     */
    public function sortByRelation(string $value)
    {
        $relations = $this->getRelationsSortMap();
        if (in_array($value, array_keys($relations))) {
            $relations[$value]($this->builder);
        }
    }
}
